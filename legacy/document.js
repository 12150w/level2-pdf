var PdfKitDocument = require('pdfkit');
var objectMerge = require('object-merge');
var Drawable = require('./drawable');
var Buffer = require('buffer');
var streamConcat = require('concat-stream');

var defaultOptions = {
	page: {
		layout: 'portrait',
		size: 'letter',
		margins: 72
	},
	footer: null,
	bufferPages: true
};

var Document = function(drawables, options) {
	options = options || {};
	
	this.footer = options.footer;
	delete options.footer;
	this.header = options.header;
	delete options.header;
	
	this.options = objectMerge(defaultOptions, options);
	this.drawables = drawables || [];
	
	// Buffered drawable list format:
	// {drawable: <DRAWABLE>, page: #, offset: {x: #, y: #}}
	this.bufferedDrawables = [];
	
	this.currentY = 0;
	this.currentPage = 0;
	this.pageCount = 0;
};

Document.prototype.render = function(stream, autoRenderDisabled) {
	var self = this;
	
	// Set up document
	this.pdf = new PdfKitDocument(this.options);
	var stream = this.pdf.pipe(stream);
	this.newPageReset();
	
	// Render the root drawables
	var masterDrawable = new Drawable();
	masterDrawable.preDraw = function() {
		return self.drawables;
	};
	this.putDrawable(masterDrawable);
	
	// Render the buffered drawables
	var _i, bufferedItem;
	for(_i=0; _i<this.bufferedDrawables.length; _i++) {
		bufferedItem = this.bufferedDrawables[_i];
		
		this.pdf.switchToPage(bufferedItem.page);
		bufferedItem.drawable.draw(this.pdf, bufferedItem.offset);
	}
	
	// Render the headers and footers
	var range = this.pdf.bufferedPageRange();
	for(_i=0; _i<range.count; _i++) {
		this.pdf.switchToPage(_i);
		this.currentPage = _i + 1;
		this.drawFooter();
		this.drawHeader();
	}
	
	if(autoRenderDisabled !== true) {
		this.end();
	}
};

Document.prototype.end = function() {
	// Finish document
	this.pdf.end();
}

Document.prototype.stream = function(res, filename) {
	filename = filename || 'document.pdf';
	this.render(streamConcat(function(data) {
		res.set('Content-Type', 'application/pdf');
		res.set('Content-Disposition', 'filename="' + filename + '" ');
		res.status(200).send(data);
	}));
};

Document.prototype.putDrawable = function(drawable) {
	var self = this,
		margins = self.pdf.page.margins,
		remainingSpan = self.getHeightSpan() - self.currentY;
	
	// Render the drawable(s)
	var actualDraw = drawable.preDraw(remainingSpan, self.getWidthSpan(), self.pdf);
	
	if(actualDraw.constructor === Array) {
		for(var _i=0; _i<actualDraw.length; _i++) {
			self.putDrawable(actualDraw[_i]);
		}
	} else {
		
		actualDraw.pdfDoc = self;
		
		// Check for new page
		if(actualDraw.height > self.getHeightSpan() - self.currentY) {
			self.pdf.addPage();
			self.newPageReset();
		}
		
		// Run drawable beforeDraw hook if needed
		var offset = {
			x: margins.left,
			y: self.currentY
		};
		if(typeof(actualDraw.options.beforeDraw) === 'function') {
			actualDraw.options.beforeDraw(self, {offset: offset});
		}
		
		if(actualDraw.isBuffered() === true) {
			this.bufferedDrawables.push({
				drawable: actualDraw,
				page: this.currentPage - 1,
				offset: offset
			});
		} else {
			actualDraw.draw(self.pdf, offset);
		}
		
		// Record the new currentY
		self.currentY += actualDraw.height;
		
	}
	
};

Document.prototype.newPageReset = function() {
	this.currentY = this.pdf.page.margins.top;
	this.currentPage = this.currentPage + 1;
	this.pageCount = this.pageCount + 1;
};

Document.prototype.getWidthSpan = function() {
	var pageWidth = this.pdf.page.width,
		margins = this.pdf.page.margins;
	return pageWidth - (margins.left + margins.right);
};

Document.prototype.getHeightSpan = function() {
	var pageHeight = this.pdf.page.height,
		margins = this.pdf.page.margins;
	return pageHeight - margins.bottom; //(margins.top + margins.bottom);
};

Document.prototype.drawFooter = function() {
	if(this.footer == null || typeof(this.footer.forEach) !== 'function') {
		return;
	}
	var self = this,
		yIndex = self.getHeightSpan();
	
	self.footer.forEach(function(footerDrawable) {
		footerDrawable.preDraw(self.pdf.page.margins.bottom, self.getWidthSpan(), self.pdf);
		footerDrawable.pdfDoc = self;
		footerDrawable.draw(self.pdf, {
			x: self.pdf.page.margins.left,
			y: yIndex
		});
		yIndex += footerDrawable.height;
	});
};

Document.prototype.drawHeader = function() {
	if(this.header == null || typeof(this.header.forEach) !== 'function') {
		return;
	}
	var self = this,
		yIndex = 0;
	
	self.header.forEach(function(footerDrawable) {
		footerDrawable.preDraw(self.pdf.page.margins.bottom, self.getWidthSpan(), self.pdf);
		footerDrawable.pdfDoc = self;
		footerDrawable.draw(self.pdf, {
			x: self.pdf.page.margins.left,
			y: yIndex
		});
		yIndex += footerDrawable.height;
	});
};

module.exports = Document;
