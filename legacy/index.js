var Document = require('./document.js'),
	Drawable = require('./drawable.js'),
	Row = require('./drawables/row.js'),
	Picture = require('./drawables/picture.js'),
	Text = require('./drawables/text.js'),
	Spacer = require('./drawables/spacer.js'),
	Line = require('./drawables/line.js'),
	NewPage = require('./drawables/new-page.js'),
	HLayout = require('./drawables/horizontal-layout'),
	VLayout = require('./drawables/vertical-layout');

var pdf = {
	drawables: {
		Row: Row,
		Picture: Picture,
		Text: Text,
		Spacer: Spacer,
		Line: Line,
		NewPage: NewPage,
		HLayout: HLayout,
		VLayout: VLayout
	}
};

pdf.Document = Document;
pdf.Drawable = Drawable;

module.exports = pdf;
