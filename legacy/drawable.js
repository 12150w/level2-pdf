/* 
 * Drawable
 *    Any element drawn to the document extends the Drawable object
 *    The default implementation prints out the object
 * 
 */
var drawableCount = 0;

/* 
 * new Drawable
 *    Constructs a new Drawable instance
 * 
 * Parameters:
 *    options - Options associated with this drawable
 * 
 */
var Drawable = function(context, options) {
	this.options = options || {};
	this.buffer = this.options.doBuffer === true;
	this.context = context;
	
	this.height = 0;
	this.count = ++drawableCount;
};

/* 
 * Pre Draw
 *    Called to measure the size of this drawable
 *    If the drawable cannot be split return the drawable
 *    If the drawable can be split return a list of sub-drawables
 * 
 *    !This function must set this.height!
 * 
 * Parameters:
 *    availableHeight - The hight (in points) available on this page for the drawable
 *    width - The width the drawable must span
 *    doc - the pdfkit document
 * 
 * Returns:
 *    A list of or a single drawable that will be drawn
 * 
 */
Drawable.prototype.preDraw = function(availableHeight, width, doc) {
	doc.fontSize(12);
	this.text = '[object Drawable:' + this.count + ']';
	this.height = doc.heightOfString(this.text);
	this.width = width;
	return this;
};

/* 
 * Draw
 *    Called to draw the drawable to the document
 *    Drawing takes place on the document and can include inserting a new page
 *    !The drawable cannot leave its bounds from preDraw!
 * 
 * Parameters:
 *    doc - the pdf document from pdfkit
 *    offset - {x:0, y:0} the offset to draw from
 * 
 */
Drawable.prototype.draw = function(doc, offset) {
	doc.text(this.text, offset.x, offset.y, {
		height: this.height
	});
};

Drawable.prototype.toString = function() {
	return '[object Drawable]';
};

Drawable.prototype.isBuffered = function() {
	//return this.buffer; // This should be used but there is some weird bug with javascript and inheritance (so we think)
	return true;
};

module.exports = Drawable;
