/* 
 * Horizontal Layout
 * 
 * Context: array of drawables columns
 * 
 * Options:
 *    borderSize: border width in points
 *    borderColor: border color
 *    width: Width of row
 *    fontSize: size of font for text cells
 * 
 */

var Row = require('./row'),
	NewPage = require('./new-page'),
	
	objectMerge = require('object-merge');

var defaultOptions = {
	borderSize: 1,
	padding: 3,
	cellWidths: [],
	backgrounds: [],
	borderColor: 'black',
	noSplit: false
};

HLayout.prototype = new Row();
function HLayout() {
	Row.apply(this, arguments);
	this.options = objectMerge(defaultOptions, this.options);
	this.context = this.context || [];
};

HLayout.prototype.preDraw = function(availableHeight, width, doc) {
	this.width = width;
	
	// Check for empty contents
	if(this.context.length < 1) {
		this.height = 0;
		return this;
	}
	
	// Measure all the content up to the availableHeight
	var _i, _j, _k, colItems, colHeight, itemSplit, subItemHeight,
		maxColHeight = 0,
		hasOverflow = false,
		overFlowItems = [],
		keepItems = [];
	for(_i=0; _i<this.context.length; _i++) {
		// Reset for the current column
		colItems = this.context[_i];
		colHeight = 0;
		overFlowItems.push([]);
		keepItems.push([]);
		
		// Measure all the column's items
		for(_j=0; _j<colItems.length; _j++) {
			itemSplit = colItems[_j].preDraw(availableHeight, this.widthForColumn(_i), doc);
			
			// Handle a minimized item
			if(itemSplit.constructor !== Array) {
				colHeight += itemSplit.height;
				if(colHeight > availableHeight) {
					overFlowItems[_i].push(itemSplit);
					hasOverflow = true;
				} else {
					keepItems[_i].push(itemSplit);
				}
			}
			
			// Handle sub-items
			else {
				subItemHeight = 0;
				for(_k=0; _k<itemSplit.length; _k++) {
					itemSplit[_k].preDraw(availableHeight - subItemHeight, this.widthForColumn(_i), doc);
					colHeight += itemSplit[_k].height;
					subItemHeight += itemSplit[_k].height;
					
					if(colHeight > availableHeight) {
						overFlowItems[_i].push(itemSplit[_k]);
						hasOverflow = true;
					} else {
						keepItems[_i].push(itemSplit[_k]);
					}
				}
			}
		}
		
		// Check if this is the largest column so far
		if(colHeight > maxColHeight) {
			maxColHeight = colHeight;
		}
	}
	
	var copyOptions = JSON.parse(JSON.stringify(this.options));
	copyOptions.noSplit = true;
	
	// Handle non-overflowing case
	if(hasOverflow !== true || this.options.noSplit === true) {
		this.height = maxColHeight;
		return this;
	}
	
	// Handle an overflow
	else {
		this.height = availableHeight;
		
		var keepLayout = new HLayout(keepItems, copyOptions);
		
		return [
			keepLayout,
			new HLayout(overFlowItems, copyOptions)
		];
	}
	
};

HLayout.prototype.draw = function(doc, offset) {
	var maxY = offset.y + this.height;
	
	// Appy the draw settings
	doc.lineWidth(this.options.borderSize);
	
	// Draw backgrounds
	if(!!this.options.backgrounds) {
		var barX = offset.x;
		for(var i=0; i<this.options.backgrounds.length; i++) {
			if(this.options.backgrounds[i] !== null) {
				doc.rect(barX, offset.y, this.widthForColumn(i) + this.options.padding*2, this.height)
				   .fill(this.options.backgrounds[i])
			}
			barX += this.widthForColumn(i) + this.options.padding*2;
		}
	}
	
	// Draw the border
	if(!!this.options.borderSize) {
		doc.lineJoin('miter')
		   .rect(offset.x, offset.y, this.width, this.height)
		   .stroke(this.options.borderColor);
		var barX = this.widthForColumn(0) + offset.x + this.options.padding*2;
		for(var i=1; i<this.context.length; i++) {
			doc.moveTo(barX, offset.y)
				.lineTo(barX, maxY)
				.stroke(this.options.borderColor);
			barX += this.widthForColumn(i) + this.options.padding*2;
		}
	}
	
	// Draw the cell contents
	var _i, _j, currentItem, drawY,
		drawX = offset.x + this.options.padding;
	for(_i=0; _i<this.context.length; _i++) {
		drawY = offset.y + this.options.padding;
		
		for(_j=0; _j<this.context[_i].length; _j++) {
			currentItem = this.context[_i][_j];
			currentItem.pdfDoc = doc;
			currentItem.draw(doc, {
				x: drawX,
				y: drawY
			});
			drawY += currentItem.height;
		}
		
		drawX += this.widthForColumn(_i);
	}
	
};

/*
HLayout.prototype.draw = function(doc, offset) {
	var maxX = offset.x + this.width,
		maxY = offset.y + this.height;
	
	// Appy the draw settings
	doc.lineWidth(this.options.borderSize);
	
	// Draw backgrounds
	if(!!this.options.backgrounds) {
		var barX = offset.x;
		for(var i=0; i<this.options.backgrounds.length; i++) {
			if(this.options.backgrounds[i] !== null) {
				doc.moveTo(barX, offset.y)
					.lineTo(barX, maxY)
					.stroke();
				doc.rect(barX, offset.y, this.widthForColumn(i) + this.options.padding*2, this.height)
				   .fill(this.options.backgrounds[i])
			}
			barX += this.widthForColumn(i) + this.options.padding*2;
		}
	}
	
	// Draw the border
	if(!!this.options.borderSize) {
		doc.lineJoin('miter')
		   .rect(offset.x, offset.y, this.width, this.height)
		   .stroke(this.options.borderColor);
		var barX = this.widthForColumn(0) + offset.x + this.options.padding*2;
		for(var i=1; i<this.context.length; i++) {
			doc.moveTo(barX, offset.y)
				.lineTo(barX, maxY)
				.stroke(this.options.borderColor);
			barX += this.widthForColumn(i) + this.options.padding*2;
		}
	}
	
	// Draw the cell contents
	var drawX = 0 + offset.x + this.options.padding;
	for(var i=0; i<this.context.length; i++) {
		var drawable = this.context[i],
			cellX = offset.x + i*this.cellWidth;
		
		if(typeof(drawable.draw) === 'function') {
			drawable.pdfDoc = this.pdfDoc;
			drawable.draw(doc, {
				x: drawX,
				y: offset.y + this.options.padding
			});
		} else {
			doc.text(drawable.toString(), drawX, offset.y + this.options.padding, {
				width: this.widthForColumn(i),
				height: this.height - this.options.padding*2
			});
		}
		
		drawX += this.widthForColumn(i) + 2*this.options.padding;
	}
	
};
*/

module.exports = HLayout;
