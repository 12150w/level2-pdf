/* 
 * Picture
 *    Draw a picture and so on
 * 
 */

var Drawable = require('../drawable'),
	objectMerge = require('object-merge'),
	imageSize = require('image-size');

var defaultOptions = {
	align: null,
	width: null,
	height: null
};

Picture.prototype = new Drawable();
function Picture() {
	Drawable.apply(this, arguments);
	this.options = objectMerge(defaultOptions, this.options);
	this.path = this.context;
};

Picture.prototype.preDraw = function(availableHeight, width, doc) {
	this.width = width;
	
	// Get image size
	var image = imageSize(this.path);
	this.imageSize = {
		width: image.width,
		height: image.height
	};
	
	// Scale imageSize as needed
	if(!!this.options.height && !this.options.width) {
		this.imageSize.width = (this.options.height/this.imageSize.height)*this.imageSize.width;
		this.imageSize.height = this.options.height;
	} else if(!!this.options.width && !this.options.height) {
		this.imageSize.height = (this.options.width/this.imageSize.width)*this.imageSize.height;
		this.imageSize.width = this.options.width;
	} else if(!!this.options.width && !!this.options.height) {
		console.log('auto none');
		this.imageSize.width = this.options.width;
		this.imageSize.height = this.options.height;
	}
	
	// Set the height
	this.height = this.imageSize.height;
	
	return this;
};

Picture.prototype.draw = function(doc, offset) {
	var imageParams = {
		height: this.imageSize.height,
		width: this.imageSize.width
	};
	
	// Align the image as needed
	var useX = offset.x,
		useY = offset.y,
		align = this.options.align;
	
	if(align === 'right') {
		useX += this.width - imageParams.width;
	} else if(align === 'center') {
		useX += (this.width/2) - (imageParams.width/2);
	}
	
	doc.image(this.path, useX, useY, imageParams);
};

module.exports = Picture;
