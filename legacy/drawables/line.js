var Drawable = require('../drawable'),
	objectMerge = require('object-merge');

var defaultOptions = {
	offset: 0,
	size: 1,
	bottom: 0,
};

Line.prototype = new Drawable();
function Line(color) {
	Drawable.apply(this, arguments);
	this.color = this.context || 'black';
	this.options = objectMerge(defaultOptions, this.options);
};

Line.prototype.preDraw = function(availableHeight, width, doc) {
	this.width = width;
	
	this.height = this.options.offset + this.options.size + this.options.bottom;
	
	return this;
};

Line.prototype.draw = function(doc, offset) {
	doc.moveTo(offset.x, offset.y + this.options.offset)
	   .lineTo(offset.x + this.width, offset.y + this.options.offset)
	   .lineWidth(this.options.size)
	   .stroke(this.color);
};

module.exports = Line;
