/* 
 * Vertical Layout
 *    Puts drawables one after another
 *    !NOTE: not page breaking yet
 *
 */
var Drawable = require('../drawable');

var VLayout = function() {
	Drawable.apply(this, arguments);
};
VLayout.prototype = new Drawable();

VLayout.prototype.preDraw = function(availableHeight, width, doc) {
	this.width = width;
	
	var subHeight = 0;
	for(var _i=0; _i<this.context.length; _i++) {
		this.context[_i].preDraw(availableHeight, width, doc);
		subHeight += this.context[_i].height;
	}
	this.height = subHeight;
	
	return this;
};

VLayout.prototype.draw = function(doc, offset) {
	var vTrack = offset.y;
	for(var _i=0; _i<this.context.length; _i++) {
		this.context[_i].draw(doc, {
			x: offset.x,
			y: vTrack
		});
		vTrack += this.context[_i].height;
	}
};

module.exports = VLayout;
