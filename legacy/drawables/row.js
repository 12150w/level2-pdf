/* 
 * Table Row
 * 
 * Use in conjunction with the Table component
 * 
 * Context: array of drawables for cells
 * 
 * Options:
 *    borderSize: border width in points
 *    borderColor: border color
 *    width: Width of row
 *    fontSize: size of font for text cells
 * 
 */

var Drawable = require('../drawable.js');
var objectMerge = require('object-merge');

var defaultOptions = {
	borderSize: 1,
	padding: 3,
	cellWidths: [],
	backgrounds: [],
	borderColor: 'black'
};

Row.prototype = new Drawable();
function Row() {
	Drawable.apply(this, arguments);
	this.options = objectMerge(defaultOptions, this.options);
	this.context = this.context || [];
};

Row.prototype.preDraw = function(availableHeight, width, doc) {
	var self = this;
	self.width = width;
	
	self.cellWidth = (self.options.width || width) / self.context.length;
	if(self.cellWidth === Infinity) {
		self.height = 0;
		return;
	}
	self.paddedCellWidth = this.cellWidth - this.options.padding*2;
	
	// Find tallest cell
	var tallestCell = self.getCellHeight(self.context[0], availableHeight, doc, self.widthForColumn(0));
	if(self.context.length > 1) {
		var columnIndex = 1;
		self.context.slice(1).forEach(function(drawable) {
			var testHeight = self.getCellHeight(drawable, availableHeight, doc, self.widthForColumn(columnIndex++));
			if(testHeight > tallestCell) {
				tallestCell = testHeight;
			}
		});
	}
	self.height = tallestCell + this.options.padding*2;
	
	return this;
	
};

Row.prototype.draw = function(doc, offset) {
	var maxX = offset.x + this.width,
		maxY = offset.y + this.height;
	
	// Appy the draw settings
	doc.lineWidth(this.options.borderSize);
	
	// Draw backgrounds
	if(!!this.options.backgrounds) {
		var barX = offset.x;
		for(var i=0; i<this.options.backgrounds.length; i++) {
			if(this.options.backgrounds[i] !== null) {
				doc.moveTo(barX, offset.y)
					.lineTo(barX, maxY)
					.stroke();
				doc.rect(barX, offset.y, this.widthForColumn(i) + this.options.padding*2, this.height)
				   .fill(this.options.backgrounds[i])
			}
			barX += this.widthForColumn(i) + this.options.padding*2;
		}
	}
	
	// Draw the border
	if(!!this.options.borderSize) {
		doc.lineJoin('miter')
		   .rect(offset.x, offset.y, this.width, this.height)
		   .stroke(this.options.borderColor);
		var barX = this.widthForColumn(0) + offset.x + this.options.padding*2;
		for(var i=1; i<this.context.length; i++) {
			doc.moveTo(barX, offset.y)
				.lineTo(barX, maxY)
				.stroke(this.options.borderColor);
			barX += this.widthForColumn(i) + this.options.padding*2;
		}
	}
	
	// Draw the cell contents
	var drawX = 0 + offset.x + this.options.padding;
	for(var i=0; i<this.context.length; i++) {
		var drawable = this.context[i],
			cellX = offset.x + i*this.cellWidth;
		
		if(typeof(drawable.draw) === 'function') {
			drawable.pdfDoc = this.pdfDoc;
			drawable.draw(doc, {
				x: drawX,
				y: offset.y + this.options.padding
			});
		} else {
			doc.text(drawable.toString(), drawX, offset.y + this.options.padding, {
				width: this.widthForColumn(i),
				height: this.height - this.options.padding*2
			});
		}
		
		drawX += this.widthForColumn(i) + 2*this.options.padding;
	}
	
};

Row.prototype.getCellHeight = function(drawable, availableHeight, doc, overrideWidth) {
	if(typeof(drawable.preDraw) === 'function') {
		drawable.preDraw(availableHeight, (overrideWidth || this.paddedCellWidth), doc);
		return drawable.height;
	} else {
		return doc.heightOfString(drawable.toString(), {width: (overrideWidth || this.paddedCellWidth)});
	}
}

Row.prototype.widthForColumn = function(index) {
	if(!!this.options.cellWidths[index]) {
		return (this.options.cellWidths[index]/100) * this.width - this.options.padding*2;
	}
	return this.paddedCellWidth;
}

Row.prototype.nonPaddedWidthForColumn = function(index) {
	if(!!this.options.cellWidths[index]) {
		return (this.options.cellWidths[index]/100) * this.width;
	}
	return this.cellWidth;
}

module.exports = Row;
