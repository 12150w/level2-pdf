
var Drawable = require('../drawable'),
	objectMerge = require('object-merge');

var defaultOptions = {
	align: 'left',
	fontSize: 12,
	font: 'Times-Roman',
	color: 'black',
	offset: 0,
	left: 0,
	height: null,
	merging: true,
	split: true,
	background: null
};

var mergePattern = /\{\[(.+?)\]\}/g,
	paragraphSplitPattern = /\n/;

function Text(context, options) {
	Drawable.call(this, context, options);
	this.text = this.context || '';
	this.options = objectMerge(defaultOptions, this.options);
};
Text.prototype = Object.create(Drawable.prototype);

Text.prototype.preDraw = function(availableHeight, width, doc) {
	this.width = width;
	doc.fontSize(this.options.fontSize);
	if(!this.options.height) {
		var measureText = this.text.length === 0 ? ' ' : this.text;
		this.height = doc.heightOfString(measureText, {width: this.width}) + this.options.offset;
	} else {
		this.height = this.options.height;
	}
	this.text = this.text.replace(/\r/g, '');
	
	// Check to see if splitting is allowed/required
	if(this.height > availableHeight && this.options.split === true && availableHeight > 0) {
		var splitList = [],
			splitText = this.text.split(paragraphSplitPattern),
			subOptions = JSON.parse(JSON.stringify(this.options));
		subOptions.split = false;
		for(var _i=0; _i<splitText.length; _i++) {
			splitList.push(new Text(splitText[_i], subOptions));
		}
		if(splitList.length === 1) return splitList[0];
		else return splitList;
	}
	
	return this;
};

Text.prototype.draw = function(doc, offset) {
	var textParams = {
			width: this.width,
			height: this.height
		},
		self = this;
	
	if(['left', 'right', 'center', 'justify'].indexOf(this.options.align) !== -1) {
		textParams.align = this.options.align;
	}
	
	if(!!this.options.font) {
		doc.font(this.options.font);
	}
	
	// Merge in options to text if needed
	var drawText = this.text;
	if(this.options.merging === true && drawText != null && typeof(drawText.replace) === 'function') {
		drawText = drawText.replace(mergePattern, function(match, code) {
			switch(code.toUpperCase()) {
				case 'PAGE':
					return self.pdfDoc.currentPage;
				case 'PAGECOUNT':
					return self.pdfDoc.pageCount;
				default:
					return '';
			}
		});
	}
	
	// Draw background if needed
	if(this.options.background != null) {
		doc.rect(offset.x, offset.y, textParams.width, textParams.height)
		   .fill(this.options.background);
	}
	doc.fontSize(this.options.fontSize)
	   .fillColor(this.options.color)
	   .text(drawText, offset.x + this.options.left, offset.y + this.options.offset, textParams);
};

module.exports = Text;
