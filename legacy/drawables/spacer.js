var Drawable = require('../drawable');

Spacer.prototype = new Drawable();
function Spacer(space, options) {
	Drawable.apply(this, arguments);
	this.space = this.context || 0;
	
	options = options || {};
	this.background = options.background;
};

Spacer.prototype.preDraw = function(availableHeight, width, doc) {
	if(this.space > availableHeight) this.height = availableHeight;
	else this.height = this.space;
	this.width = width;
	
	return this;
};

Spacer.prototype.draw = function(doc, offset) {
	if(this.background != null) {
		doc.rect(offset.x, offset.y, this.width, this.height)
		   .fill(this.background);
	}
};

module.exports = Spacer;
