var Drawable = require('../drawable');

NewPage.prototype = new Drawable();
function NewPage() {
	Drawable.apply(this, arguments);
};

NewPage.prototype.preDraw = function(availableHeight, width, doc) {
	this.height = availableHeight;
	return this;
};

NewPage.prototype.draw = function(doc, offset) {
};

module.exports = NewPage;
