var pdf = require('./');
var fs = require('fs');

var drawableList = [];
for(var i=0; i<5; i++) {
	drawableList.push(new pdf.Drawable());
}
drawableList.push(new pdf.drawables.Row([
	'Hey doc hey hey hey hey hey hey hey hey hey hey',
	new pdf.drawables.Row([
		'Left left left left left left',
		'Right'
	], {
		borderSize: 0.5,
		cellWidths: [25, 75]
	}),
	new pdf.Drawable()
], {
	borderSize: 0
}));
drawableList.push(new pdf.drawables.Row([
	'A 1',
	'B 2',
	'C 3'
], {
	borderSize: 0,
	padding: 10,
}));

var count = 0;
for(var i=0; i<30; i++) {
	drawableList.push(new pdf.drawables.Row([
		'Row ' + (++count),
		'what\'s',
		new pdf.Drawable()
	], {
		cellWidths: [20, 20, 60]
	}));
}

var exampleDoc = new pdf.Document(drawableList);
var outStream = fs.createWriteStream('./example.pdf');

exampleDoc.render(outStream);
