/*
	Document Test
	
	Tests the Document interface
*/
var TestDocument = require('./lib/test-document'),
	l2 = require('../src/index'),
	assert = require('assert');

describe('Document', function() {
	
	it('sets the page dimensions', function(done) {
		var doc = new TestDocument({
			width: 72,
			height: 144
		});
		
		doc.finish().then(function(pdf) {
			var size = pdf.Root.Pages.Kids[0].MediaBox;
			assert.ok(size[2] === 72);
			assert.ok(size[3] === 144);
		}).then(function() { done(); }, done).done();
	});
	
	describe('finish()', function() {
		
		it('generates a PDF', function(done) {
			var doc = new TestDocument();
			
			doc.finish().then(function(pdf) {
				assert.ok(pdf.Root.Pages.Kids.length === 1);
			}).then(function() { done(); }, done).done();
		});
		
	});
	
	describe('add()', function() {
		
		it('puts content in the PDF', function(done) {
			var doc = new TestDocument();
			doc.add(new l2.Line(0, 0, 100, 100));
			
			doc.pageContents(0).then(function(contents) {
				assert.ok(contents.length > 1);
			}).then(function() { done(); }, done).done();
		});
		
		it('page breaks with long content', function(done) {
			var doc = new TestDocument({
				width: 72,
				height: 72,
				margins: {
					top: 11,
					bottom: 11,
					left: 11,
					right: 11
				}
			});
			for(var i=0; i<10; i++) {
				doc.add(new l2.Line(0, 0, 50, 10));
			}
			
			doc.pageContents(1).then(function(contents) {
				assert.ok(contents.length > 1);
			}).then(function() { done(); }, done).done();
		})
		
	});
	
});