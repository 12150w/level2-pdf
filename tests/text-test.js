/*
	Text Drawable Test
*/
var TestDocument = require('./lib/test-document'),
	Text = require('../src/drawables/text'),
	Line = require('../src/drawables/line');

describe('Text', function() {
	
	it('draws text on the page', function(done) {
		var doc = new TestDocument({
			width: 200,
			height: 200,
			margin: {left: 0, top: 0, right: 0, bottom: 0}
		});
		doc.add(new Line(0, 0, 100, 0));
		doc.add(new Text('Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there Hello there '));
		doc.add(new Line(0, 0, 100, 0));
		
		return doc.save().then(function() { done(); }, done).done();
		doc.pageContents(0).then(function(contents) {
			console.log(contents);
		}).then(function() { done(); }, done).done();
	});
	
});