/*
	Parsed PDF
	
	Contains a PDF document and its parsed objects
*/
var Class = require('level2-base').Class,
	ParseError = require('./parse-error'),
	zlib = require('zlib'),
	q = require('q');

var ESCAPE_CHARS = {
	'\\n': '\n',
	'\\r': '\r',
	'\\t': '\t',
	'\\b': '',
	'\\f': '',
	'\\(': '(',
	'\\)': ')',
	'\\\\': '\\'
};

module.exports = Class.extend({
	
	// init sets up the PDF
	init: function(data) {
		this._data = data;
	},
	
	// parse extracts the objects from the generated PDF file and returns a promise that resolves when finished parsing.
	parse: function(data) {
		var self = this;
		
		return q.fcall(function() {
			self._parseTrailer();
			
			// Parse the main dictionary
			return self._parseDict(self._trailer.dictStart);
		});
	},
	
	// _getLine retuns a single line from the data.
	_getLine: function(offset, backwards) {
		if(backwards == null) {
			backwards = false;
		}
		
		var stopIdx = offset;
		while(true) {
			if(stopIdx < 0) break;
			if(stopIdx > this._data.length) break;
			
			// Check for the next line
			var segment;
			if(backwards) {
				if(stopIdx > 0) segment = this._data.slice(stopIdx - 1, stopIdx);
				else segment = this._data.slice(stopIdx, stopIdx);
			} else {
				if(stopIdx < this._data.length) segment = this._data.slice(stopIdx, stopIdx + 1);
				else segment = this._data.slice(stopIdx, stopIdx);
			}
			
			if(segment.toString('hex') === '0a') {
				break;
			}
			
			if(backwards) stopIdx--;
			else stopIdx++;
		}
		
		if(backwards) {
			return {
				data: this._data.slice(stopIdx, offset),
				start: stopIdx,
				end: offset
			};
		} else {
			return {
				data: this._data.slice(offset, stopIdx),
				start: offset,
				end: stopIdx
			};
		}
	},
	
	// _parseTrailer parses the trailer data into this._trailer;
	_parseTrailer: function() {
		var idx = this._data.length;
		this._trailer = {};
		
		// Verify the EOF marker
		var endMarker = this._getLine(idx - 1, true);
		if(endMarker.data.toString('ascii') !== '%%EOF') {
			var markerText = endMarker.data.toString('hex').toUpperCase();
			throw new ParseError('Invalide EOF marker: 0x' + markerText);
		}
		idx = endMarker.start;
		
		// Parse the xref location
		while(this._trailer.xrefStart == null) {
			var nextLine = this._getLine(idx - 1, true);
			
			if(nextLine.data.toString('ascii') === 'startxref') {
				var xrefLine = this._getLine(idx);
				this._trailer.xrefStart = parseInt(xrefLine.data.toString('ascii'), 10);
			}
			
			idx = nextLine.start;
		}
		
		// Parse the xref size
		while(this._trailer.xrefSize == null) {
			var nextLine = this._getLine(idx - 1, true);
			
			if(nextLine.data.toString('ascii').indexOf('/Size') === 0) {
				this._trailer.xrefSize = parseInt(nextLine.data.toString('ascii').replace('/Size ', ''), 10);
			}
			
			idx = nextLine.start;
		}
		
		// Parse the xref table
		this._trailer.xref = this._parseXref(this._trailer.xrefStart, this._trailer.xrefSize);
		this._trailer.xrefBuf = {};
		
		// Find the trailer dictionary start
		while(this._trailer.dictStart == null) {
			var nextLine = this._getLine(idx - 1, true);
			
			if(nextLine.data.toString('ascii') === 'trailer') {
				this._trailer.dictStart = nextLine.end + 1;
			}
			
			idx = nextLine.start;
		}
	},
	
	// _parseXref parses the xref table given the xref location and size.
	_parseXref: function(offset, size) {
		var xref = {};
		
		// Verify xref start tag
		var startLine = this._getLine(offset);
		if(startLine.data.toString('ascii') !== 'xref') {
			throw new ParseError('Invalid xref start tag "' + startLine.data.toString('ascii') + '" at ' + offset);
		}
		
		// Parse all the entries
		var read = 0,
			idx = startLine.end;
		while(read < size) {
			var nextLine = this._getLine(idx + 1),
				idx = nextLine.end,
				groupData = /^(\d+) (\d+)$/.exec(nextLine.data.toString('ascii'));
			
			if(groupData == null) {
				throw new ParseError('Invalid xref group "' + nextLine.data.toString('ascii') + '"');
			}
			var groupIdx = parseInt(groupData[1], 10),
				groupSize = parseInt(groupData[2], 10);
			
			for(var i=0; i<groupSize; i++) {
				var entry = this._getLine(idx + 1),
					entryData = /^(\d+) (\d+) ([nf]) $/.exec(entry.data.toString('ascii'));
				idx = entry.end;
				
				if(entryData == null) {
					throw new ParseError('Invalid xref entry "' + entry.data.toString('ascii') + '"');
				}
				
				xref[groupIdx + '.' + parseInt(entryData[2], 10)] = {
					offset: parseInt(entryData[1], 10),
					type: entryData[3]
				};
				
				groupIdx++;
				read++;
			}
		}
		
		return xref;
	},
	
	// _parseDict parses dictionary data from the stream.
	// start is the index of the beginning of the dictionary starting tag (<<).
	_parseDict: function(start, _searchStack) {
		var dict = {};
		
		// Verify starting tag
		var startLine = this._getLine(start);
		if(startLine.data.toString('ascii') !== '<<') {
			throw new ParseError('Invalid dictionary start "' + startLine.data.toString('ascii') + '" at ' + start);
		}
		
		var idx = startLine.end;
		while(true) {
			var nextLine = this._getLine(idx + 1);
			
			// Check for end tag
			if(nextLine.data.toString('ascii').trim() === '>>') {
				break;
			}
			
			// Parse the key and data beginning
			var entryData = /^\s*\/([^ ]+) /.exec(nextLine.data.toString('ascii'));
			if(entryData == null) {
				throw new ParseError('Invalid dictionary key "' + nextLine.data.toString('ascii') + '"');
			}
			var key = entryData[1],
				dataStart = nextLine.start + 1 + key.length + 1;
			
			// Parse the data
			dict[key] = this._parseGeneric(dataStart, _searchStack);
			
			idx = nextLine.end;
		}
		
		// Check if this is a stream
		var streamLine = this._getLine(nextLine.end + 1);
		if(streamLine.data.toString('ascii') === 'stream') {
			var streamSize = dict.Length,
				streamFilter = dict.Filter;
			
			// Verify we can read the stream
			if(streamSize == null) {
				throw new ParseError('Unknown stream size ' + dict);
			}
			if(streamFilter !== 'FlateDecode') {
				throw new ParseError('This parser does not support the stream filter ' + streamFilter);
			}
			
			// Load the stream
			var streamData = this._data.slice(streamLine.end + 1, streamLine.end + 1 + streamSize);
			dict._stream = zlib.inflateSync(streamData);
		}
		
		return dict;
	},
	
	// _parseGeneric parses a single object from the data stream.
	// Note that the _searchStack argument is used for indirect object lookup and should be omitted.
	_parseGeneric: function(offset, _searchStack) {
		var dataLine = this._getLine(offset),
			lineText = dataLine.data.toString('ascii');
		
		// Check for name val
		var staticMatch = /^\/([^\s\]]+)/.exec(lineText);
		if(staticMatch != null) {
			return staticMatch[1];
		}
		
		// Check for string val
		if(lineText.indexOf('(') === 0) {
			var stringVal = '',
				idx = offset + 1,
				
				inEscape = false,
				parenthLevel = 0;
			
			while(true) {
				var nextChar = this._data.slice(idx, idx + 1).toString('ascii');
				
				// Check if this is an escaped character
				if(inEscape === true) {
					inEscape = false;
					idx++;
					
					var escapeVal = ESCAPE_CHARS['\\' + nextChar];
					if(escapeVal == null) {
						throw new ParseError('Invalid escaped character "\\' + nextChar + '"');
					}
					stringVal += escapeVal;
					
					continue;
				}
				
				// Check for end
				if(nextChar === ')' && parenthLevel < 1) {
					break;
				}
				
				// Check for escape start
				if(nextChar === '\\') {
					inEscape = true;
					idx++;
					continue;
				}
				
				// Check for parenthesis level change
				if(nextChar === '(') {
					parenthLevel++;
				} else if(nextChar === ')') {
					parenthLevel--;
				}
				
				stringVal += nextChar;
				idx++;
			}
			
			return stringVal;
		}
		
		// Check for indirect reference
		var indirectMatch = /^(\d+) (\d+) R/.exec(lineText);
		if(indirectMatch != null) {
			return this._lookupIndirect(indirectMatch[1], indirectMatch[2], _searchStack);
		}
		
		// Check for number
		var numMatch = /^([\+-]?\d*\.?\d*)/.exec(lineText);
		if(numMatch != null) {
			var numVal = parseFloat(numMatch[1]);
			if(!isNaN(numVal)) {
				return numVal;
			}
		}
		
		// Check for dictionary
		if(lineText.trim() === '<<') {
			return this._parseDict(offset, _searchStack);
		}
		
		// Check for array
		if(lineText.trim()[0] === '[') {
			return this._parseArray(offset, _searchStack);
		}
		
		throw new ParseError('Invalid generic data type "' + lineText + '"');
	},
	
	// _lookupIndirect resolves an indirect reference value.
	_lookupIndirect: function(objNum, objGen, _searchStack) {
		var refKey = objNum + '.' + objGen,
			ref = this._trailer.xref[refKey];
		
		// verify this reference is defined
		if(ref == null) {
			throw new ParseError('Could not find xref for ' + refKey + ' in parsed xref table');
		}
		
		// check for circular reference
		_searchStack = _searchStack == null ? [] : _searchStack;
		if(_searchStack.indexOf(refKey) > -1) {
			return function() {
				return this.trailer.xrefBuf[this.key]
			}.bind({
				trailer: this._trailer,
				key: refKey
			});
		}
		_searchStack.push(refKey);
		
		// verify ref is to a valid object by checking the referenced line
		var refLine = this._getLine(ref.offset),
			refData = /^(\d+) (\d+) obj/.exec(refLine.data.toString('ascii'));
		if(refData == null) {
			throw new ParseError('Invalid xref to ' + refKey + ': "' + refLine.data.toString('ascii') + '"');
		}
		
		var resolvedVal = this._parseGeneric(refLine.end + 1, _searchStack);
		this._trailer.xrefBuf[refKey] = resolvedVal;
		return resolvedVal;
	},
	
	// _parseArray parses array values.
	_parseArray: function(offset, _searchStack) {
		var arrayLine = this._getLine(offset),
			arrayText = arrayLine.data.toString('ascii'),
			arrayVal = [];
		
		// Check start character
		if(arrayText.indexOf('[') !== 0) {
			throw new ParseError('Invalid array start "' + arrayText + '"');
		}
		
		// Break up array into items
		var items = [],
			idx = offset + 1,
			currentVal = '';
		while(true) {
			var currentChar = this._data.slice(idx, idx + 1).toString('ascii');
			
			// Check for array end
			if(currentChar === ']') {
				this._addArrayItem(items, currentVal);
				break;
			}
			
			// Check for delimiter
			if(/^\s$/.exec(currentChar) != null) {
				this._addArrayItem(items, currentVal);
				currentVal = '';
			}
			
			// Add to current val
			else {
				currentVal += currentChar;
			}
			
			idx++;
		}
		
		// Load array items
		var loadIdx = 1;
		for(var i = 0; i < items.length; i++) {
			var itemVal = this._parseGeneric(offset + loadIdx, _searchStack);
			arrayVal.push(itemVal);
			
			loadIdx += items[i].length + 1;
		}
		
		return arrayVal;
	},
	
	// _addArrayItem pushes an array item into an array, checking for indirect references
	_addArrayItem: function(arr, item) {
		if(item !== 'R') {
			return arr.push(item);
		}
		
		// load the full indirect reference
		var refText = arr[arr.length - 2] + ' ' + arr[arr.length - 1] + ' R';
		arr.splice(arr.length - 2, 2, refText);
	},
	
});