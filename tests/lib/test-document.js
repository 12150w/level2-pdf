/*
	Test Document
	
	A Document implementation that generates a test-able description
	instead of a PDF output.
*/
var Document = require('../../src/document'),
	ParsedPDF = require('./parsed-pdf'),
	fs = require('fs'),
	q = require('q');

module.exports = Document.extend({
	
	// init sets up the test document
	init: function() {
		this._super.apply(this, arguments);
		this._contents = [];
		
		var self = this;
		this.on('data', function(chunk) {
			self._contents.push(chunk);
		});
	},
	
	// finish ends the document and returns a promise that resolves with the PDF data.
	finish: function() {
		var finishDefer = q.defer(),
			self = this;
		
		// Wait for the document to end
		this.on('end', function() {
			var pdf = new ParsedPDF(Buffer.concat(self._contents));
			pdf.parse().then(finishDefer.resolve, finishDefer.reject);
		});
		this.on('error', finishDefer.reject);
		
		this._super.apply(this, arguments);
		return finishDefer.promise;
	},
	
	// pageContents loads the page contents (graphics PS) for a certain page with a promise.
	pageContents: function(pageIndex) {
		return this.finish().then(function(data) {
			// Load the page
			var pageCount = data.Root.Pages.Kids.length;
			if(pageIndex + 1 > pageCount) {
				throw new Error('PDF only has ' + pageCount + ' pages (requested page ' + (pageIndex + 1) + ')');
			}
			return data.Root.Pages.Kids[pageIndex];
			
		}).then(function(page) {
			// Parse the contents
			var contents = page.Contents._stream.toString('ascii').split(/\n/),
				commands = [];
			
			contents.forEach(function(cmdText) {
				if(cmdText == '') return;
				var cmdParts = cmdText.split(' ');
				
				commands.push({
					op: cmdParts[cmdParts.length - 1],
					args: cmdParts.slice(0, cmdParts.length - 1)
				});
			});
			
			return commands;
		});
	},
	
	// save stores the pdf on the filesystem for manual verification
	save: function(path) {
		path = path || 'test.pdf';
		var self = this;
		
		return this.finish().then(function() {
			// Save the PDF
			return q.nfcall(fs.writeFile, path, Buffer.concat(self._contents));
			
		});
	}
	
});