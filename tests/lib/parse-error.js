/*
	ParseError
	
	Thrown when unable to parse a PDF
*/
var L2Error = require('level2-base').Error;

module.exports = L2Error.extend({
	name: 'ParseError'
});