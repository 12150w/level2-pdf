/*
	Text Drawable
	
	Draws text to the PDF
*/
var Drawable = require('../drawable');

module.exports = Drawable.extend({
	
	// init sets up the drawable
	init: function(text, options) {
		this.text = text;
		this._opts = options || {};
	},
	
	// size sizes the entire text contents
	size: function(doc) {
		if(this._opts.width == null) {
			this._opts.width = doc._opts.width - (doc._opts.margins.left + doc._opts.margins.right);
		}
		var textHeight = doc.kit.heightOfString(this.text, this._opts);
		
		return {
			x: this._opts.width,
			y: 0
		};
	},
	
	// draw draws the text
	draw: function(doc) {
		doc.kit.text(this.text, 0, 0, this._opts);
	}
	
});