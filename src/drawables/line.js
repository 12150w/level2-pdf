/*
	Line Drawable
	
	Draws a single line
*/
var Drawable = require('../drawable');

module.exports = Drawable.extend({
	
	// init sets up the line.
	init: function(startX, startY, endX, endY) {
		this._start = {x: startX, y: startY};
		this._end = {x: endX, y: endY};
	},
	
	// draw draws the line.
	draw: function(doc) {
		doc.kit.moveTo(this._start.x, this._start.y)
			.lineTo(this._end.x, this._end.y)
			.stroke();
	},
	
	// sizes the line
	size: function(doc) {
		return {
			x: Math.abs(this._end.x - this._start.x),
			y: Math.abs(this._end.y - this._start.y)
		};
	}
	
});