/*
	Drawable
	
	Interface that describes content in a PDF
*/
var Class = require('level2-base').Class;

module.exports = Class.extend({
	
	// split is called before drawing to break the drawable into multiple drawables.
	// If this drawable does not split don't override this method.
	split: function(doc) {
		return this;
	},
	
	// size is called before drawing to get the drawable dimensions (bounding box).
	// If this drawable only contains other drawables, don't implement this method (it won't be called).
	// This must return an object with an "x" and "y" properties which are numbers.
	size: function(doc) {
		throw new Error('size() is not implemented');
	},
	
	// draw adds this drawable to the PDF.
	// If this drawable only contains other drawables, don't implement this method (it won't be called).
	draw: function(doc) {
		throw new Error('draw() is not implemented');
	}
	
});