/*
	Document
	
	The context of a PDF document containing Drawables.
	A Document implements the ReadableStream interface.
*/
var Class = require('level2-base').Class,
	ReadableStream = require('stream').Readable,
	PDFKit = require('pdfkit');

module.exports = Class.extendGeneric(ReadableStream, {
	
	// init sets up the document.
	init: function(options) {
		this._opts = options || {};
		
		// Set up the options
		this._opts.margins = this._opts.margins || {};
		this._opts.margins.top = this._opts.margins.top || 72;
		this._opts.margins.bottom = this._opts.margins.bottom || 72;
		this._opts.margins.left = this._opts.margins.left || 72;
		this._opts.margins.right = this._opts.margins.right || 72;
		
		this._opts.width = this._opts.width || 8.5*72;
		this._opts.height = this._opts.height || 11*72;
		
		// Set up the PDF
		this._drawables = [];
		this.kit = new PDFKit({
			size: [this._opts.width, this._opts.height],
			margins: this._opts.margins
		});
		this.location = {
			x: 0,
			y: 0
		};
		
		this.kit.on('pageAdded', function() {
			this._pageReset();
		}.bind(this));
	},
	
	// add inserts a drawable into the document at the current location.
	add: function(drawable) {
		var self = this,
			splitDrawables = drawable.split(this);
		
		if(splitDrawables === drawable) {
			this._inject(drawable);
		} else {
			splitDrawables.forEach(function(subDrawable) {
				self.add(subDrawable);
			});
		}
	},
	
	// _inject draws the drawable in the PDF
	_inject: function(drawable) {
		
		// Size the drawable
		var bounds = drawable.size(this),
			used = this._opts.margins.top + this.location.y + this._opts.margins.bottom,
			remaining = this._opts.height - (used + bounds.y);
		
		// Check for new page
		if(remaining < 0) {
			this.kit.addPage();
		}
		
		// Draw the line
		this.kit.save()
			.translate(this._opts.margins.left + this.location.x, this._opts.margins.top + this.location.y);
		drawable.draw(this);
		this.kit.restore();
		
		// Move the current location
		this.location.y += bounds.y;
	},
	
	// _pageReset resets the layout for the new page
	_pageReset: function() {
		this.location.y = 0;
	},
	
	// finish closes the drawable stream and finalizes the output (the "end" event will be fired).
	finish: function() {
		this.kit.end();
	},
	
	// _read proxies PDFKit _read
	_read: function() {
		this.kit._read.apply(this.kit, arguments);
	},
	
	// on proxies PDFKit on
	on: function() {
		this.kit.on.apply(this.kit, arguments);
	}
	
});